﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler;
using System;
using System.Security.Claims;

namespace Resource.Utils
{
    public class TicketGenerator
    {
        public const string Issuer = "Fake", OriginalIssuer = "Injection";

        private Claim identifier, name;
        
        public TicketGenerator():this("232349609592884ABCru4Fake", "John Fake"){}

        public TicketGenerator(string identifier, string name)
        {
            Identifier = identifier;
            Name = name;
        }

        public string Identifier
        {
            set
            {
                identifier=new Claim(ClaimTypes.NameIdentifier, value, ClaimValueTypes.String, Issuer, OriginalIssuer);
            }

            get { return identifier.Value; }
        }

        public string Name
        {
            set
            {
                name = new Claim(ClaimTypes.Name, value, ClaimValueTypes.String, Issuer, OriginalIssuer);
            }

            get { return name.Value; }
        }

        public Nullable<DateTime> ExpiresAt { get; set; }

        public Nullable<DateTime> IssuedAt { get; set; }

        public string Build()
        {
            return (new TicketDataFormat(new CustomDataProtector()).Protect(
                new AuthenticationTicket(
                    new ClaimsIdentity(new[] { identifier, name }, "Cookies"),
                    new AuthenticationProperties {
                        IsPersistent = false,
                        ExpiresUtc = ExpiresAt.GetValueOrDefault(DateTime.UtcNow.AddYears(1)),
                        IssuedUtc = IssuedAt.GetValueOrDefault(DateTime.UtcNow)
                    }
                )
            ));
        }
    }
}