﻿using Microsoft.Owin.Security.DataProtection;
using System.Web.Security;

namespace Resource.Utils
{
    /**
    <summary>
        This class is a DataProtector implementation for encryption/decryption of AuthenticationTicket from CookieAuthentication middleware.
    </summary>
    <remarks>It should be used along with TicketDataFormat class.</remarks>
    **/
    public class CustomDataProtector : IDataProtector
    {
        /**
        <remarks>The CookieAuthentication middleware uses these values as "purposes".</remarks>
        **/
        private static string[] purposes = new string[] {
            "Microsoft.Owin.Security.Cookies.CookieAuthenticationMiddleware",
            "Cookies",
            "v1"
        };

        public byte[] Protect(byte[] userData)
        {
            return MachineKey.Protect(userData, purposes);
        }

        public byte[] Unprotect(byte[] protectedData)
        {
            return MachineKey.Unprotect(protectedData, purposes);
        }
    }
}