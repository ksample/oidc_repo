﻿namespace Resource.Utils
{
    public class MessageWrapper<T>
    {
        public bool Success { get; set; }
        public T Msg { get; set; }
    }
}