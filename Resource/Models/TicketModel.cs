﻿using System.Collections.Generic;

namespace Resource.Models
{
    public class TicketModel
    {
        public bool IsAuthenticated { get; set; }
        public string Issuer { get; set; }
        public string IssuedUtc { get; set; }
        public virtual IDictionary<string,string> Claims { get; set; }

        public override bool Equals(object obj)
        {
            if (!(obj is TicketModel)) return false;
            TicketModel that = obj as TicketModel;
            bool claims = true;
            claims &= Claims.Count == that.Claims.Count;
            if (!claims) return false;
            foreach(var k in Claims.Keys) claims &= Claims[k].Equals(that.Claims[k]);
        
            return (claims && IsAuthenticated.Equals(that.IsAuthenticated) &&
                    IssuedUtc.Equals(that.IssuedUtc) &&
                    Issuer.Equals(that.Issuer));
        }

        public override int GetHashCode()
        {
            int result=17;
            result = (result * 3371) ^ IsAuthenticated.GetHashCode();
            result = (result * 3371) ^ (IssuedUtc==null?0:IssuedUtc.GetHashCode());
            result = (result * 3371) ^ (Issuer==null?0:Issuer.GetHashCode());
            foreach (var v in Claims.Values) result = (result * 3371) ^ (v==null?0:v.GetHashCode());
            return result;
        }
    }
}