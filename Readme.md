OpenID Connect (OIDC) sample
===========


This work is an example that shows how to set up the Katana's[^katana] OpenIdConnect middleware for [implicit flow](http://openid.net/specs/openid-connect-core-1_0.html#ImplicitFlowAuth) along with the Katana's Cookie middleware. It's also an example for the "Applying cookie-stored sessions in web farms with ASP.NET and OpenID Connect 1.0" article, available at [here](https://dzone.com/articles/applying-cookie-stored-sessions-in-web-farms-with). There, you can find all conceptual content involved in this work.


## Description

The solution has three projects: GoogleOpenIdSample, OIDC_Test and Resource.

- *GoogleOpenIdSample* is an ASP.NET MVC 5 project, which requires SSL/TLS configuration and uses the Google's OpenId Provider along with Cookie and OpenIdConnect middlewares. You may set the OpenIdConnect's *RedirectUri* property in the **Startup** class and the *GoogleCID* in the **Web.config** file according to your environment[^gca]. However, if you run this project keeping the original setup, you don't need to modify *GoogleCID* or *RedirectUri*. This means that you can carry out it on your localhost machine over the 44336 port.

	**Note:** the *DebugController's* actions only exist on Debug mode. I use the **#if DEBUG** preprocessor for this.

- *OIDC_Test* is a test suite for *GoogleOpenIdSample* with basic and integrated tests. There are two fundamental tests in the **EssentialTest** class, in which **MachineKeyTest** is the main. If that test fails, any other will fail too. Therefore, to avoid problems, keep the *OIDC_Test's* **machineKey** element equals to the *GoogleOpenIdSample's* **machineKey** element; moreover, you must set the *Host* parameter according to your environment. Both are in the **app.config** file.

- Finally, the *Resource* project is a library that both *GoogleOpenIdSample* and *OIDC_Test* share.


## Executing the tests

To carry out the tests, you must keep the *GoogleOpenIdSample* in Debug mode and run it without debugging (Ctrl+F5). Then, you can run the *OIDC_Test* from the Test Explorer*.


## About Licenses

This software uses the MIT's license terms, which is found at the **LICENSE** file.

[^katana]: [Katana](https://github.com/aspnet/AspNetKatana) is the Microsoft implementation for the [OWIN](http://owin.org/html/spec/owin-1.0.html) specification.

[^gca]: If you want to create your own project in Google Console API, see the [Setting up OAuth 2.0](https://developers.google.com/identity/protocols/OpenIDConnect#registeringyourapp) section.