﻿using System.Web.Mvc;

namespace GoogleOpenIdSample.Controllers
{
    public class CommonController : Controller
    {
        public ActionResult Index(){ return RedirectPermanent("/dynamic/account"); }

        [Route("404")]
        public ActionResult NotFound(){
            ViewBag.Title = ViewBag.Message = "Resource not found";
            Response.StatusCode = 404;
            return View("Common");
        }

        [Route("50x")]
        public ActionResult ServerError() {
            ViewBag.Title = "Internal error";
            ViewBag.Message = "Sorry, an internal error has occurred at the server.";
            Response.StatusCode = 500;
            return View("Common");
        }

    }
}