﻿using System.Web.Optimization;

namespace GoogleOpenIdSample
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js"));
            bundles.Add(new ScriptBundle("~/bundles/jqueryUI").Include("~/Scripts/jquery-ui.js"));
            bundles.Add(new StyleBundle("~/bundles/theme").IncludeDirectory("~/Content","*.css"));
        }
    }
}