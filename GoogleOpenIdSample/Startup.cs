﻿using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security;
using System.Configuration;
using Microsoft.Owin.Security.OpenIdConnect;
using System.IdentityModel.Tokens;
using System.Security.Claims;

[assembly: OwinStartup(typeof(GoogleOpenIdSample.Startup))]
namespace GoogleOpenIdSample
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                LoginPath = new PathString("/dynamic/account"),
                AuthenticationMode = AuthenticationMode.Active,
                CookieSecure = CookieSecureOption.Always,
                CookieHttpOnly = true,
                CookiePath = "/dynamic"
            });

            app.UseOpenIdConnectAuthentication(new OpenIdConnectAuthenticationOptions
            {
                ClientId = ConfigurationManager.AppSettings["GoogleCID"],
                AuthenticationType = "Google",
                Authority = "https://accounts.google.com",
                AuthenticationMode = AuthenticationMode.Passive,
                Scope = "openid profile email",
                /* 
                This value must be equal to that registered in console.developers.google.com
                Indeed, OIDC middleware doesn't call this action when we use response_type=id_token.
                However, the Google's OP requires it.
                */
                RedirectUri = "https://localhost:44336",

                ResponseType = "id_token",

                // Google advises to validate Audiences and Issuers
                TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = "https://accounts.google.com",
                    ValidateIssuer = true,
                    ValidAudience = ConfigurationManager.AppSettings["GoogleCID"],
                    ValidateAudience = true
                },

                Notifications = new OpenIdConnectAuthenticationNotifications
                {
                    // At this moment, ID Token has already been validated.
                    SecurityTokenValidated = (ctx) =>
                    {
                        // We can remove claims that are not necessary in this context, mitigating the cookie size.
                        var identity = ctx.AuthenticationTicket.Identity;
                        identity.TryRemoveClaim(identity.FindFirst("azp"));
                        identity.TryRemoveClaim(identity.FindFirst("aud"));
                        identity.TryRemoveClaim(identity.FindFirst("nonce"));
                        identity.TryRemoveClaim(identity.FindFirst("email_verified"));
                        identity.TryRemoveClaim(identity.FindFirst("picture"));
                        identity.TryRemoveClaim(identity.FindFirst("jti"));
                        identity.TryRemoveClaim(identity.FindFirst("iss"));
                        identity.TryRemoveClaim(identity.FindFirst("exp"));
                        identity.TryRemoveClaim(identity.FindFirst("iat"));

                        // Adjusting the claims' type according to WIF standard.
                        var tClaim = identity.FindFirst("name");
                        if (tClaim != null)
                        {
                            identity.AddClaim(new Claim(ClaimTypes.Name, tClaim.Value, tClaim.ValueType, tClaim.Issuer, tClaim.OriginalIssuer));
                            identity.RemoveClaim(tClaim);
                        }
                        tClaim = identity.FindFirst("locale");
                        if (tClaim != null)
                        {
                            identity.AddClaim(new Claim(ClaimTypes.Locality, tClaim.Value, tClaim.ValueType, tClaim.Issuer, tClaim.OriginalIssuer));
                            identity.RemoveClaim(tClaim);
                        }

                        return Task.FromResult(0);
                    }
                }
            });
        }
    }
}
