﻿using Resource.Utils;
using System;
using System.Text;
using System.Web.Helpers;
using System.Web.Mvc;

namespace GoogleOpenIdSample.Filters
{
    /**
    <summary>This filter validates the anti-forgery token, which comes from an Ajax request.</summary>
    <remarks>The Ajax request must provide the anti-forgery token in a HTTP header field, so-called "X-Requested-With".</remarks>
    **/
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class XhrValidateAntiForgeryToken : FilterAttribute, IAuthorizationFilter
    {
        private JsonResult AntiForgeryFail()
        {
            return new JsonResult
            {
                ContentEncoding = Encoding.UTF8,
                ContentType = "application/json",
                RecursionLimit = 2,
                Data = new MessageWrapper<string>{ Success = false, Msg = "The anti-forgery validation has failed." }
            };
        }

        public const string ANTI_FORGERY_HEADER = "X-Requested-With";

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var tkFromCookie = filterContext.HttpContext.Request.Cookies[AntiForgeryConfig.CookieName];
            string tkFromHeader = filterContext.HttpContext.Request.Headers[ANTI_FORGERY_HEADER];

            if (tkFromCookie==null || String.IsNullOrWhiteSpace(tkFromHeader)){
                filterContext.Result = this.AntiForgeryFail();
                return;
            }

            try{ AntiForgery.Validate(tkFromCookie.Value, tkFromHeader); }
            catch (HttpAntiForgeryException){ filterContext.Result = this.AntiForgeryFail(); }
        }

    }
}