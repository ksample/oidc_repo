﻿using System;
using System.Web.Mvc;
using Microsoft.Owin.Security.DataHandler;
using GoogleOpenIdSample.Filters;
using System.Collections.Generic;
using Resource.Utils;
using Resource.Models;

namespace GoogleOpenIdSample.Areas.Dynamic.Controllers
{
    [Authorize]
    [RouteArea("dynamic")]
    [RoutePrefix("resource")]
    [Route("{action=index}")]
    public class ResourceController : Controller
    {
        // GET: Resource
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Decryption()
        {
            return View();
        }

        [HttpPost]
        [XhrValidateAntiForgeryToken]
        public ActionResult DataDecryption(string cypher)
        {
            var defaultAnswer = new MessageWrapper<string>{ Success=false, Msg = "This ticket is invalid." };
            if (String.IsNullOrWhiteSpace(cypher)) return Json(defaultAnswer);

            var ticketGen = new TicketDataFormat(new CustomDataProtector());
            var tk = ticketGen.Unprotect(cypher);
            if (tk == null) return Json(defaultAnswer);

            IDictionary<string, string> claims = new Dictionary<string, string>();
            foreach(var c in tk.Identity.Claims)claims.Add(c.Type, c.Value);
            var it = tk.Identity.Claims.GetEnumerator(); it.MoveNext();
            var finalObj = new TicketModel
            {
                IsAuthenticated = tk.Identity.IsAuthenticated,
                Issuer = it.Current.Issuer,
                IssuedUtc = tk.Properties.IssuedUtc.Value.ToString("yyyy-MM-dd HH:mm:ss"),
                Claims = claims
            };

            return Json(new MessageWrapper<TicketModel> { Success = true, Msg = finalObj });
        }

    }
}