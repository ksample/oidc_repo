﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using System.Web;
using System.Web.Mvc;

namespace GoogleOpenIdSample.Areas.Dynamic.Controllers
{
    [RouteArea("dynamic")]
    [RoutePrefix("account")]
    [Route("{action=index}")]
    public class AccountController : Controller
    {
        
        public ActionResult Index(string returnUrl)
        {
            if (User.Identity.IsAuthenticated){
                if (!Url.IsLocalUrl(returnUrl) || returnUrl.StartsWith("/Dynamic"))
                    return RedirectToAction("Index", "Resource");

                return Redirect(returnUrl);
            }

            ViewBag.ReturnUrl = returnUrl;
            Response.Cache.SetNoStore();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void Login(string returnUrl)
        {
            if (!Url.IsLocalUrl(returnUrl)) returnUrl = Url.Action("Index", "Resource");
            // The authenticationType's value must be the same defined into Startup class
            HttpContext.GetOwinContext().Authentication.Challenge(
                new AuthenticationProperties{RedirectUri = returnUrl}, "Google"
            );
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            HttpContext.GetOwinContext().Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return RedirectToAction("Index", "Resource");
        }

    }
}