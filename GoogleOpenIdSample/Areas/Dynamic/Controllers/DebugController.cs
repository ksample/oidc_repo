﻿/*
 * This controller only exists on Debug mode and its purpose is to provide resources for the automatic test.
 */
#if DEBUG
using Microsoft.Owin.Security.DataHandler;
using Resource.Utils;
using System.Web.Helpers;
using System.Web.Mvc;

namespace GoogleOpenIdSample.Areas.Dynamic.Controllers
{
    [RouteArea("dynamic")]
    [RoutePrefix("debug")]
    [Route("{action=index}")]
    public class DebugController : Controller
    {
        [Authorize]
        public ActionResult CSRFToken()
        {
            string forCookie, forHeader;
            AntiForgery.GetTokens(null, out forCookie, out forHeader);
            return Json(
                new MessageWrapper<object> { Success = true, Msg = new { TCookie = forCookie, THeader = forHeader } }, 
                JsonRequestBehavior.AllowGet
            );
        }

        [HttpPost]
        public ActionResult MachineKeyValidation(string tk)
        {
            var utk=new TicketDataFormat(new CustomDataProtector()).Unprotect(tk);
            MessageWrapper<string> ret;
            if (utk == null) ret = new MessageWrapper<string> { Success = false, Msg = "Bad configuration for machine key" };
            else ret = new MessageWrapper<string> { Success = true, Msg = "Good, the machine keys are equal." };

            return Json(ret, JsonRequestBehavior.AllowGet);
        }
    }

}
#endif