﻿using System;
using System.Configuration;

namespace OIDC_Test
{
    public static class Setup
    {
        public static readonly Uri BaseAddress = new Uri(ConfigurationManager.AppSettings["Host"]);
    }
}
