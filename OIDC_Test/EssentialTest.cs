﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using static OIDC_Test.Setup;
using Resource.Utils;

namespace OIDC_Test
{
    [TestCategory("Basic")]
    [TestClass]
    public class EssentialTest
    {
        public TestContext TestContext { get; set; }

        [Description(@"
         SCENARIO: The machine-key element must be the same at both Here and Server.
         GIVEN this machine key is equal to the machine key at Server.
         WHEN I submit an encrypted data to the Server.
         THEN the Server must be able to decrypt that data.")]
        [TestMethod]
        public void MachineKeyTest()
        {
            // Arrange
            var tk = new TicketGenerator().Build();
            MessageWrapper<string> mw;

            // Act
            using (var client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false }))
            {
                client.BaseAddress = BaseAddress;
                var response = client.PostAsync(
                    "/dynamic/debug/machinekeyvalidation",
                    new FormUrlEncodedContent(new[] {
                        new KeyValuePair<string,string>("tk", tk)
                    })
                ).Result;
                response.EnsureSuccessStatusCode();
                mw = JsonConvert.DeserializeObject<MessageWrapper<string>>(
                    response.Content.ReadAsStringAsync().Result
                );

                TestContext.WriteLine(mw.Msg);
            }

            // Assert
            Assert.IsTrue(mw.Success);
        }

        [Description(@"
         SCENARIO: Authorize filter validation.
         GIVEN an unauthenticated user agent.
         WHEN it tries to access a protected resource.
         THEN the Authorize filter must redirect it to the login page.")]
        [TestMethod]
        public void AuthTest()
        {
            // Arrange and Act
            HttpResponseMessage resp=null;
            
            using (var client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false }))
            {
                client.BaseAddress = BaseAddress;
                resp = client.GetAsync("/dynamic/resource").Result;
            }
            
            // Assert
            Assert.IsTrue(
                resp.StatusCode == HttpStatusCode.Redirect &&
                resp.Headers.Location.AbsolutePath.Contains("dynamic/account")
            );
        }

    }
}
