﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Security.Claims;
using static OIDC_Test.Setup;
using Resource.Utils;
using Resource.Models;

namespace OIDC_Test
{
    [TestCategory("Integration")]
    [TestClass]
    public class IntegrationTest
    {
        protected static Cookie[] cookies = new Cookie[2];
        protected static string HeaderToken;
        protected static TicketGenerator CurrentUser = new TicketGenerator("Piko1293", "Mark Down"){
            IssuedAt = DateTime.UtcNow,
            ExpiresAt = DateTime.UtcNow.AddHours(1)
        };
        protected CookieContainer GetBasicCookies(){
            var cont = new CookieContainer(3);
            cont.Add(BaseAddress, cookies[0]); cont.Add(BaseAddress, cookies[1]);
            return cont;
        }

        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public static void Initialize(TestContext tc)
        {

            cookies[0] = new Cookie {
                Name = ".AspNet.Cookies",
                Value = CurrentUser.Build(),
                Path = "/dynamic",
                Secure = true,
                HttpOnly = true
            };

            using (var handler = new HttpClientHandler { CookieContainer = new CookieContainer(1), AllowAutoRedirect = false })
            using (var client = new HttpClient(handler))
            {
                handler.CookieContainer.Add(BaseAddress, cookies[0]);
                client.BaseAddress = BaseAddress;
                HttpResponseMessage response = client.GetAsync("/dynamic/debug/csrftoken").Result;
                response.EnsureSuccessStatusCode();
                var json = JsonConvert.DeserializeObject<MessageWrapper<Dictionary<string, string>>>(
                    response.Content.ReadAsStringAsync().Result
                );

                HeaderToken = json.Msg["THeader"];
                cookies[1] = new Cookie
                {
                    Name = "__RequestVerificationToken",
                    Value = json.Msg["TCookie"],
                    Secure = true,
                    HttpOnly = true
                };
            }
        }

        [Description(@"
         SCENARIO: DataDecryption action must correctly decrypt a valid ticket.
         GIVEN an authenticated user agent AND a valid encrypted AuthenticationTicket.
         WHEN it is submitted to DataDecryption.
         THEN DataDecryption must decrypt it.")]
        [TestMethod]
        public void DataDecryptionSuccessful()
        {
            // Arrange
            TicketModel expected = new TicketModel {
                IsAuthenticated=true,
                IssuedUtc=CurrentUser.IssuedAt.GetValueOrDefault().ToString("yyyy-MM-dd HH:mm:ss"),
                Issuer=TicketGenerator.Issuer,
                Claims=new Dictionary<string, string>{
                    {ClaimTypes.NameIdentifier, CurrentUser.Identifier},
                    {ClaimTypes.Name, CurrentUser.Name}
                }
            };

            MessageWrapper<TicketModel> received=null;

            // Act
            using (var handler = new HttpClientHandler { CookieContainer = this.GetBasicCookies(), AllowAutoRedirect = false })
            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = BaseAddress;
                client.DefaultRequestHeaders.Add("X-Requested-With", HeaderToken);
                var response = client.PostAsync("/dynamic/resource/datadecryption",
                    new FormUrlEncodedContent(new[]{
                        new KeyValuePair<string, string>("cypher", cookies[0].Value)
                    })
                ).Result;
                response.EnsureSuccessStatusCode();
                received = JsonConvert.DeserializeObject<MessageWrapper<TicketModel>>(
                    response.Content.ReadAsStringAsync().Result
                );
            }

            // Assert
            Assert.IsNotNull(received);
            Assert.IsTrue(received.Success);
            Assert.AreEqual(expected, received.Msg);
        }

        [Description(@"
         SCENARIO: DataDecryption action must not decrypt an invalid ticket.
         GIVEN an authenticated user agent AND an invalid encrypted AuthenticationTicket.
         WHEN it is submitted to DataDecryption.
         THEN DataDecryption must return the following error message: This ticket is invalid.")]
        [DataRow("JHGSJDO9098489HFI287D73228", "This ticket is invalid.")] // invalid ticket
        // valid, but encrypted with other machine key
        [DataRow(@"3ldPmPzwoXZ2GKbLy8Mknp-8528qwvbP4s3d2RN7weHIlLBjJ7nwZyF12XUa-BOuOthKEJhesBN9k2NzR9X9vZ5ge7CrV841h6
                 pg3xulMKYtRAcGCpfHpZjM4NvuHKJiSlUvtPnNuBvqH2A4TUQ3JH5Vv2IL-wfI5uLKn55FOTrshcR6mqgoxRQq8MGvYMl_0XH4p
                 _zfhZRHGroubJYbK1JZTguJXFKBSWBWUBu7VIaj4B_VWnefMrfAHw0C0C7YiIiWS5jvGfr6T_5JSM1zp5gZ7eUw_nQ6_DF8lkfr
                 0lplxDaGphMtEmu0W7HDNjsdvQQFk58FjFj18GWDISj2-w", "This ticket is invalid.")]
        [DataRow("", "This ticket is invalid.")] // empty string
        [DataRow(" ", "This ticket is invalid.")] // blank space
        [DataTestMethod]
        public void DataDecryptionFail(string ticket, string expected)
        {
            // Arrange
            MessageWrapper<string> received = null;

            // Act
            using (var handler = new HttpClientHandler { CookieContainer = this.GetBasicCookies(), AllowAutoRedirect = false })
            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = BaseAddress;
                client.DefaultRequestHeaders.Add("X-Requested-With", HeaderToken);
                var response = client.PostAsync("/dynamic/resource/datadecryption",
                    new FormUrlEncodedContent(new[]{
                        new KeyValuePair<string, string>("cypher", ticket)
                    })
                ).Result;
                response.EnsureSuccessStatusCode();
                received = JsonConvert.DeserializeObject<MessageWrapper<string>>(
                    response.Content.ReadAsStringAsync().Result
                );
            }

            // Assert
            Assert.IsNotNull(received);
            Assert.IsFalse(received.Success);
            Assert.AreEqual(expected, received.Msg);
        }

        [Description(@"
         SCENARIO: XhrValidateAntiForgeryToken validation for invalid anti-forgery cookie.
         GIVEN an authenticated user agent AND an invalid anti-forgery cookie AND a valid token into the HTTP header.
         WHEN both cookie and header are submitted to the Xhr filter.
         THEN the Xhr filter must block the request AND returns a notification message.")]
        [DataRow("", "The anti-forgery validation has failed.")]
        [DataRow(" ", "The anti-forgery validation has failed.")]
        [DataRow("LJOIJ3409H33OD93IKD99H328BEBMCNW", "The anti-forgery validation has failed.")]
        [DataTestMethod]
        public void XhrFilterValidationCookie(string token, string expected)
        {
            // Arrange
            MessageWrapper<string> received = null;

            // Act
            using (var handler = new HttpClientHandler { CookieContainer=GetBasicCookies(), AllowAutoRedirect = false })
            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = BaseAddress;
                handler.CookieContainer.Add(BaseAddress, new Cookie {
                    Name=cookies[1].Name,
                    Secure=true,
                    HttpOnly=true,
                    Value=token
                });
                client.DefaultRequestHeaders.Add("X-Requested-With", HeaderToken);
                var response = client.PostAsync("/dynamic/resource/datadecryption",
                    new FormUrlEncodedContent(new[]{
                        new KeyValuePair<string, string>("cypher", cookies[0].Value)
                    })
                ).Result;
                response.EnsureSuccessStatusCode();
                received = JsonConvert.DeserializeObject<MessageWrapper<string>>(
                    response.Content.ReadAsStringAsync().Result
                );
            }

            // Assert
            Assert.IsNotNull(received);
            Assert.IsFalse(received.Success);
            Assert.AreEqual(expected, received.Msg);
        }

        [Description(@"
         SCENARIO: XhrValidateAntiForgeryToken validation for invalid anti-forgery token into the HTTP header
         GIVEN an authenticated user agent AND a valid anti-forgery cookie AND an invalid token into the HTTP header
         WHEN both cookie and header are submitted to the Xhr filter.
         THEN the Xhr filter must block the request AND returns a notification message.")]
        [DataRow("", "The anti-forgery validation has failed.")]
        [DataRow(" ", "The anti-forgery validation has failed.")]
        [DataRow("LJOIJ3409H33OD93IKD99H328BEBMCNW", "The anti-forgery validation has failed.")]
        [DataTestMethod]
        public void XhrFilterValidationHeader(string token, string expected)
        {
            // Arrange
            MessageWrapper<string> received = null;

            // Act
            using (var handler = new HttpClientHandler { CookieContainer = GetBasicCookies(), AllowAutoRedirect = false })
            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = BaseAddress;
                client.DefaultRequestHeaders.Add("X-Requested-With", token);
                var response = client.PostAsync("/dynamic/resource/datadecryption",
                    new FormUrlEncodedContent(new[]{
                        new KeyValuePair<string, string>("cypher", cookies[0].Value)
                    })
                ).Result;
                response.EnsureSuccessStatusCode();
                received = JsonConvert.DeserializeObject<MessageWrapper<string>>(
                    response.Content.ReadAsStringAsync().Result
                );
            }

            // Assert
            Assert.IsNotNull(received);
            Assert.IsFalse(received.Success);
            Assert.AreEqual(expected, received.Msg);
        }

        [Description(@"
         SCENARIO: User gets out.
         GIVEN an authenticated user agent.
         WHEN it calls the LogOff action.
         THEN the LogOff redirects it to 'resource/index' AND invalidates the '.AspNet.Cookies' cookie")]
        [TestMethod]
        public void LogoffTest()
        {
            // Arrange and Act
            HttpResponseMessage resp;
            string[] cookies;

            using (var handler = new HttpClientHandler { AllowAutoRedirect = false, CookieContainer = GetBasicCookies() })
            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = BaseAddress;
                resp = client.PostAsync(
                    "/dynamic/account/logoff",
                    new FormUrlEncodedContent(new[] {
                        new KeyValuePair<string,string>("__RequestVerificationToken", HeaderToken)
                    })
                ).Result;
            }

            // Assert
            Assert.IsNotNull((cookies = ((string[])resp.Headers.GetValues("Set-Cookie"))));
            Assert.AreEqual(1, cookies.Length);
            Assert.AreEqual(resp.StatusCode, HttpStatusCode.Redirect);
            Assert.IsTrue(cookies[0].Contains(".AspNet.Cookies=;"));
        }
    }
}
